const Level = Object.freeze({EASY: "easy", MEDIUM: "medium", HARD: "hard", CUSTOM: "custom"});

const easyConfig = {
    rows: 9,
    columns: 9, 
    mines: 9, 
    protectFirstClick: true, 
    enableMarks: true
};

const mediumConfig = {
    rows: 16,
    columns: 16, 
    mines: 40, 
    protectFirstClick: true, 
    enableMarks: true
};

const hardConfig = {
    rows: 16,
    columns: 30, 
    mines: 99, 
    protectFirstClick: true, 
    enableMarks: true
};

let currentConfig;
let currentGame;

(function ($) {
    $.fn.minesweeper = function() {
        if (currentGame != undefined) {
            currentGame.clear();
        }
        currentGame = new Game(currentConfig);
        currentGame.initGame();
        return this;
    };
}(jQuery));

$(document).ready(function () {
    $("#level-change").click(function() {
        let target = $("#settings-change");
        if (target.css("display") === "none") {
            target.slideDown();
        } else {
            target.slideUp();
        }
    });

    $("#level").on("change", function(){
        let level = $("#level").val();
        updateConfig();
        if (level === Level.CUSTOM) {
            $("#settings-custom").slideDown();
        } else {
            $("#settings-custom").slideUp();
        }
    });

    $("#protect-first-click").on("change", function() {
        updateConfig();
    });

    $("#enable-doubt-marks").on("change", function() {
        updateConfig();
    });

    updateConfig();
});

function updateConfig() {
    switch($("#level").val()) {
        case Level.EASY:
            currentConfig = easyConfig;
            preloadCustomConfig();
            break;
        case Level.MEDIUM:
            currentConfig = mediumConfig;
            preloadCustomConfig();
            break;
        case Level.HARD:
            currentConfig = hardConfig;
            preloadCustomConfig();
            break;
        case Level.CUSTOM:
            currentConfig = readCustomConfig();
            break;
    }
    readOtherSettings();
    $('#minesweeper').minesweeper();
}

function readCustomConfig() {
    let rows = $("#custom-rows").val();
    let columns = $("#custom-columns").val();
    let mines = $("#custom-mines").val();
    if (mines >= rows * columns) {
        mines = rows * columns - 1;
        $("#custom-mines").val(mines);
    }
    return {
        rows: rows,
        columns: columns,
        mines: mines
    }
}

function preloadCustomConfig() {
    $("#custom-rows").val(currentConfig.rows);
    $("#custom-columns").val(currentConfig.columns);
    $("#custom-mines").val(currentConfig.mines);
}

function readOtherSettings() {
    let protectFirstClick = $("#protect-first-click").is(':checked');
    let enableMarks = $("#enable-doubt-marks").is(':checked');
    currentConfig["protectFirstClick"] = protectFirstClick;
    currentConfig["enableMarks"] = enableMarks;
}
