class Timer {
    constructor(element) {
        this.element = element;
        this.interval;
    }

    start() {
        this.interval = setInterval(this.update(Date.now(), this.element), 1000);
    }

    update(startTime, element) {
        return function() {
            let secondsElapsed = Math.floor((Date.now() - startTime) / 1000);
            if (secondsElapsed <= 999) {
                element.text(("00" + secondsElapsed).slice(-3));
            } else {
                this.stop();
            }
        }
    }

    stop() {
        if (this.interval) {
            clearInterval(this.interval);
            this.interval = null;
            this.startTime = null;
        }
    }

    reset() {
        this.element.text("000");
    }
}