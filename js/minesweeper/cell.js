class Cell {
    constructor(element, i, j) {
        this.element = element;
        this.row = i;
        this.column = j;

        this.covered = true;
        this.mine = false;
        this.flagged = false;
        this.doubt = false;
        this.minesCount = -1;
    }

    getRow() {
        return this.row;
    }

    getColumn() {
        return this.column;
    }

    getElement() {
        return this.element;
    }

    isCovered() {
        return this.covered;
    }

    isMine() {
        return this.mine;
    }

    setMine(isMine) {
        this.mine = isMine;
    }

    getMinesCount() {
        return this.minesCount;
    }

    setMinesCount(minesCount) {
        this.minesCount = minesCount;
    }

    isFlagged() {
        return this.flagged;
    }

    setFlagged(flagged) {
        this.flagged = flagged;
        this.doubt = false;
        this.element.removeClass("doubt");
        this.element.empty();
        if (this.flagged) {
            this.element.addClass("flagged")
            this.addIcon();
        } else {
            this.element.removeClass("flagged")
        }
    }

    isDoubt() {
        return this.doubt;
    }

    setDoubt(doubt) {
        this.doubt = doubt;
        this.flagged = false;
        this.element.removeClass("flagged")
        this.element.empty();
        if (this.doubt) {
            this.element.addClass("doubt")
            this.addIcon();
        } else {
            this.element.removeClass("doubt")
        }
    }

    isEmpty() {
        return this.minesCount === -1;
    }

    reveal() {
        if (this.covered) {
            if (this.mine) {
                this.element.addClass("mine");
                this.addIcon();
            } else if (this.minesCount > 0) {
                this.element.addClass(`mine-count-${this.minesCount}`);
    
                let span = $(`<span>${this.minesCount}</span>`)
                this.element.append(span);
            }
            this.element.removeClass("covered");
            this.covered = false;
        }
    }

    highlight(highlight) {
        if (highlight && this.covered && !this.flagged && !this.doubt) {
            this.element.addClass("highlight");
        } else {
            this.element.removeClass("highlight");
        }
    }

    addIcon() {
        if (this.flagged) {
            this.element.text("🚩");
        } else if (this.doubt) {
            this.element.text("❓");
        } else if (this.mine) {
            this.element.text("💣");
        }
    }
}
