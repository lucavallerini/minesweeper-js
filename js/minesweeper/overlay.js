const OverlayType = Object.freeze({DEFAULT: 0, POSITIVE: 1, NEGATIVE: 2});

class Overlay {
    constructor(element) {
        this.element = element;
    }

    showOverlay(message, type) {
        this.element.text(message);
        if (type === OverlayType.POSITIVE) {
            this.element.addClass("positive");
        } else if (type === OverlayType.NEGATIVE) {
            this.element.addClass("negative");
        } else {
            this.element.addClass("default");
        }
    }

    hideOverlay() {
        this.element.text("");
        this.element.removeClass("default positive negative");
    }
}