class MinesCounter {
    constructor(element, mines) {
        this.element = element;
        this.mines = mines;

        this.updateMinesCount(0);
    }

    updateMinesCount(flags) {
        let remainingMines = this.mines - flags;
        if (remainingMines >= 0) {
            this.element.text(("00" + remainingMines).slice(-3));
        } else if (remainingMines < 0 && remainingMines >= -9) {
            this.element.text(("-0" + -remainingMines).slice(-3));
        } else {
            this.element.text(remainingMines);
        }
    }
}