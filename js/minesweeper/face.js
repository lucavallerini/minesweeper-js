const FaceEnum = Object.freeze({DEFAULT: "🙂", LOOSER: "😵", WINNER: "😎", FEARFUL: "😨"});

class Face {
    constructor(element) {
        this.element = element;
    }

    setFace(face) {
        this.element.text(face);
    }
}
