class Game {
    constructor(config) {
        this.rows = config.rows;
        this.columns = config.columns;
        this.mines = config.mines;
        this.protectFirstClick = config.protectFirstClick;

        this.board;

        this.hasGameStarted = false;
        this.hasGameFinished = false;
        this.timer = new Timer($("#timer"));
        this.minesCounter = new MinesCounter($("#mines"), this.mines);

        let faceElement = $("#face");
        faceElement.click(this.handleClickOnFace(this));
        this.face = new Face(faceElement);

        this.overlay = new Overlay($("#overlay"));
    }

    initGame() {
        this.resetGame();
    }

    gameStarted() {
        if (!this.hasGameStarted) {
            this.hasGameStarted = true;
            this.timer.start();
        }
    }

    gameFinished(won) {
        if (won) {
            this.face.setFace(FaceEnum.WINNER);
            this.overlay.showOverlay("YOU WON!", OverlayType.POSITIVE);
        } else {
            this.face.setFace(FaceEnum.LOOSER);
            this.overlay.showOverlay("YOU LOST!", OverlayType.NEGATIVE);
        }
        this.timer.stop();
        this.hasGameStarted = false;
        this.hasGameFinished = true;
    }

    updateMinesCounter(flags) {
        this.minesCounter.updateMinesCount(flags);
    }

    handleClickOnFace(game) {
        return function() {
            game.resetGame();
        }
    }

    handleFace(face) {
        this.face.setFace(face);
    }

    resetGame() {
        this.clear();
        this.board = new Board(this, this.rows, this.columns, this.mines);
    }

    clear() {
        this.updateMinesCounter(0);
        this.timer.stop();
        this.timer.reset();
        this.face.setFace(FaceEnum.DEFAULT);
        this.hasGameStarted = false;
        this.hasGameFinished = false;
        this.overlay.hideOverlay();
    }
}
