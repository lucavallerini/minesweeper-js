class Board {
    constructor(game, rows, columns, mines) {
        this.game = game;
        this.rows = rows;
        this.columns = columns;
        this.mines = mines;
        this.board = [];
        this.uncoveredCells = 0;
        this.flags = 0;

        this.highlightThreatsModeOn = false;
        this.highlightThreatsOfCell = null;

        $("#boardgame").empty();
        $("#boardgame").mouseleave(this, function(event) {
            let board = event.data;
            if (!board.game.hasGameFinished) {
                if (board.highlightThreatsModeOn) {
                    board.stopHighlightMode();
                } else {
                    board.game.handleFace(FaceEnum.DEFAULT);
                }
            }
        });

        this.drawBoard();
    }

    drawBoard() {
        for (let i = 0; i < this.rows; i++) {
            this.board[i] = [];
            let rowElement = $('<div class="row" />');
            for (let j = 0; j < this.columns; j++) {
                let boardElement = $('<div class="cell covered" />');
                let cell = new Cell(boardElement, i, j);
                this.board[i][j] = cell;
                this.setupEventHandlers(cell);
                rowElement.append(boardElement);
            }
            $("#boardgame").append(rowElement);
        }
    }

    setupEventHandlers(cell) {
        let boardElement = cell.getElement();
        boardElement.click(this.handleLeftClick(this, cell));
        boardElement.contextmenu(this.handleRightClick(this, cell));
        boardElement.mousedown({arg1: this, arg2: cell}, function(event) {
            let board = event.data.arg1;
            let cell = event.data.arg2;
            if (!board.game.hasGameFinished) {
                if (event.which === 2) {
                    board.startHighlightMode(cell);
                } else if (event.which === 1) {
                    board.game.handleFace(FaceEnum.FEARFUL);
                }
            }
        });
        boardElement.mouseup({arg1: this, arg2: cell}, function(event) {
            let board = event.data.arg1;
            let cell = event.data.arg2;
            if (!board.game.hasGameFinished) {
                if (event.which === 2) {
                    board.stopHighlightMode(cell);
                } else if (event.which === 1) {
                    board.game.handleFace(FaceEnum.DEFAULT);
                }
            }
        });
        boardElement.mouseover({arg1: this, arg2: cell}, function(event) {
            let board = event.data.arg1;
            let cell = event.data.arg2;
            if (!board.game.hasGameFinished) {
                if (board.highlightThreatsModeOn) {
                    board.updateHighlighMode(cell);
                }
            }
        });
    }

    generateRandomIndex(max) {
        return Math.floor(Math.random() * max) % max;
    }

    placeMines(iToProtect, jToProtect) {
        let minesPlaced = 0;
        while (minesPlaced < this.mines) {
            let i = this.generateRandomIndex(this.rows);
            let j = this.generateRandomIndex(this.columns);

            if (iToProtect === i && jToProtect === j) {
                continue;
            }

            let cell = this.board[i][j];
            if (!cell.isMine()) {
                cell.setMine(true);
                minesPlaced++;
            }
        }

        this.computeMines();
    }

    areValidIndexes(i, j) {
        return i >= 0 && j >= 0 && i < this.rows && j < this.columns;
    }

    computeAdjacientMines(cell) {
        let mines = 0;
        for (let i = -1; i <= 1; i++) {
            for (let j = -1; j <= 1; j++) {
                let targetI = cell.getRow() + i;
                let targetJ = cell.getColumn() + j;
                if (this.areValidIndexes(targetI, targetJ)) {
                    let targetCell = this.board[targetI][targetJ];
                    if (targetCell.isMine()) {
                        mines++;
                    }
                }
            }
        }
        return mines;
    }

    computeMines() {
        for (let i = 0; i < this.rows; i++) {
            for (let j = 0; j < this.columns; j++) {
                let cell = this.board[i][j];
                if (!cell.isMine()) {
                    let mines = this.computeAdjacientMines(cell);
                    if (mines > 0) {
                        cell.setMinesCount(mines);
                    }
                }
            }
        }
    }

    floodReveal(cell) {
        // top cell
        let targetI = cell.getRow() - 1;
        let targetJ = cell.getColumn();
        if (this.areValidIndexes(targetI, targetJ)) {
            this.revealCell(this.board[targetI][targetJ]);
        }
        
        // bottom cell
        targetI = cell.getRow() + 1;
        targetJ = cell.getColumn();
        if (this.areValidIndexes(targetI, targetJ)) {
            this.revealCell(this.board[targetI][targetJ]);
        }

        // left cell
        targetI = cell.getRow();
        targetJ = cell.getColumn() - 1;
        if (this.areValidIndexes(targetI, targetJ)) {
            this.revealCell(this.board[targetI][targetJ]);
        }

        // right cell
        targetI = cell.getRow();
        targetJ = cell.getColumn() + 1;
        if (this.areValidIndexes(targetI, targetJ)) {
            this.revealCell(this.board[targetI][targetJ]);
        }

        // top left cell
        targetI = cell.getRow() - 1;
        targetJ = cell.getColumn() - 1;
        if (this.areValidIndexes(targetI, targetJ)) {
            this.revealCell(this.board[targetI][targetJ]);
        }

        // top right cell
        targetI = cell.getRow() - 1;
        targetJ = cell.getColumn() + 1;
        if (this.areValidIndexes(targetI, targetJ)) {
            this.revealCell(this.board[targetI][targetJ]);
        }

        // bottom left cell
        targetI = cell.getRow() + 1;
        targetJ = cell.getColumn() - 1;
        if (this.areValidIndexes(targetI, targetJ)) {
            this.revealCell(this.board[targetI][targetJ]);
        }

        // bottom right cell
        targetI = cell.getRow() + 1;
        targetJ = cell.getColumn() + 1;
        if (this.areValidIndexes(targetI, targetJ)) {
            this.revealCell(this.board[targetI][targetJ]);
        }
    }

    revealCell(cell, revealMine) {
        if (cell.isCovered()) {
            if (cell.isFlagged() || cell.isDoubt()) {
                return;
            } else if (cell.isMine()) {
                if (revealMine) {
                    cell.reveal();
                    this.uncoveredCells++;
                    this.game.gameFinished(false);
                }
                return;
            } else {
                cell.reveal();
                this.uncoveredCells++;

                if (cell.isEmpty()) {
                    this.floodReveal(cell);
                }
            }

            if (this.isBoardCompleted()) {
                this.game.gameFinished(true);
            }
        }
    }

    highlightAdjacientThreats(cell, highlight) {
        let threats = this.findThreats(cell);
        if (threats != undefined) {
            for (let i = 0; i < threats.length; i++) {
                threats[i].highlight(highlight);
            }
        }
    }

    findThreats(cell) {
        let threats;
        if (cell.isCovered() || !cell.isEmpty()) {
            threats = [];
            for (let i = -1; i <= 1; i++) {
                for (let j = -1; j <= 1; j++) {
                    let threatI = cell.getRow() + i;
                    let threatJ = cell.getColumn() + j;
                    if (this.areValidIndexes(threatI, threatJ) && !(threatI === cell.getRow() && threatJ === cell.getColumn())) {
                        threats.push(this.board[threatI][threatJ]);
                    }
                }
            }
        }
        return threats;
    }

    handleLeftClick(board, cell) {
        return function() {
            if (!board.game.hasGameFinished) {
                if (!board.game.hasGameStartedd) {
                    if (board.game.protectFirstClick) {
                        board.placeMines(cell.getRow(), cell.getColumn());
                    } else {
                        board.placeMines();
                    }
                    board.game.gameStarted();
                }
                board.revealCell(cell, true);
            }
        }
    }

    handleRightClick(board, cell) {
        return function() {
            if (cell.isCovered() && !board.game.hasGameFinished) {
                if (cell.isFlagged()) {
                    cell.setDoubt(true);
                    board.flags--;
                } else {
                    if (cell.isDoubt()) {
                        cell.setDoubt(false);
                    } else {
                        cell.setFlagged(true);
                        board.flags++;
                    }
                }
                board.game.updateMinesCounter(board.flags);
            }
            return false;
        }
    }

    startHighlightMode(cell) {
        this.highlightThreatsModeOn = true;
        this.updateHighlighMode(cell);
        this.game.handleFace(FaceEnum.FEARFUL);
    }

    updateHighlighMode(cell) {
        if (this.highlightThreatsOfCell != undefined) {
            this.highlightAdjacientThreats(this.highlightThreatsOfCell, false);
        }
        this.highlightThreatsOfCell = cell;
        this.highlightAdjacientThreats(cell, true);
    }

    stopHighlightMode(cell) {
        this.highlightThreatsModeOn = false;
        if (cell === undefined) {
            this.highlightAdjacientThreats(this.highlightThreatsOfCell, false);
        } else {
            this.highlightAdjacientThreats(cell, false);
        }
        this.highlightThreatsOfCell = null;
        this.game.handleFace(FaceEnum.DEFAULT);
    }

    isBoardCompleted() {
        return this.uncoveredCells + this.mines === this.rows * this.columns;
    }
}
